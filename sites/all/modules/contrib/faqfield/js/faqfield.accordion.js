/**
 * @file
 * Binds accordion to provided faq fields.
 */

(function ($) {
  /**
   * Add faqfield accordion behaviour.
   */
  Drupal.behaviors.faqfieldAccordion = {
    attach: function (context, settings) {

      if (settings.faqfield != undefined) {

        // Bind the accordion to any defined faqfield accordion formatter with
		 $("div[id*='faqfield_field']").accordion({
            collapsible: true,
			heightStyle: "content"
			}); 
        // provided settings.
        for (var selector in settings.faqfield) {
          var specs = settings.faqfield[selector];
          $(selector, context).accordion(specs);
          $( ".selector" ).accordion({
            collapsible: true,
			heightStyle: "content"
          });   
          
        }

      }

    }
  };

})(jQuery);


