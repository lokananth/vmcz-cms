<?php
/**
 * Function to API call using Curl lib
 * @param type $arr_param
 * @return type
 */
 
 define('DMNL_PAYM_PLAN_LIST', 'http://192.168.2.102:9706/api/v1/Paymgetplandetails');
  
 define('DMNL_DD_BANK_LIST', 'http://192.168.2.102:9706/api/v1/BankDetails');
 define('DMNL_DD_VALIDATE', 'http://www.bankaccountchecker.com/listener.php');
 define('DMNL_DD_GOCARD_NEWCUS_ENROLL', 'http://80.74.225.227:9003/GoCard.svc/goCardnewcustomer');
 define('DMNL_DD_GOCARD_NEWBANK_ACC_ENROLL', 'http://80.74.225.227:9003/GoCard.svc/goCardnewbankAccount');
 define('DMNL_DD_GOCARD_NEWMANDATE_ENROLL', 'http://80.74.225.227:9003/GoCard.svc/goCardNewMandate');
 
 define('DMNL_PAYMONTHLY_PAYMENT_STEP1', 'http://192.168.2.102:9706/api/v1/CTACCTopupStep1');
 define('DMNL_PAYMONTHLY_PAYMENT_STEP2', 'http://192.168.2.102:9706/api/v1/CTACCTopupStep2');
 define('DMNL_INSERT_DD_ORDER', 'http://192.168.2.102:9706/api/v1/InsertDirectDebitOrder');
 define('DMNL_PAYM_ACTIVITY_LOG', 'http://192.168.2.102:9706/api/v1/Writelog');
 define('DMNL_INSERT_NEW_SUBSCRIBER', 'http://192.168.2.102:9706/api/v1/InsertNewSubscriber');
 define('DMNL_UPDATE_EXISTING_SUBSCRIBER', 'http://192.168.2.102:9706/api/v1/UpdateExistSubscriber');
 define('DMNL_INSERT_SIM_ORDER', 'http://192.168.2.102:9706/api/v1/InsertFreeSIMwithlogin');
 
 define('DMNL_DD_DETAILS_INSERT_CRM', 'http://192.168.2.102:9706/api/v1/InsertDirectDebitAccount');
 define('DMNL_PAYG_TO_PAYM_SUBSCRIBE', 'http://192.168.2.102:9706/api/v1/DelightWEBpostpaidsubscribe'); 
 
 define('DMNL_PAYM_NEW_ORDER_DETAIL', 'http://192.168.2.102:9706/api/v1/AddNewOrderdetail'); 


 
  function apiPostPayM($apiUrl, $data = Array(), $isAuth = true) {

	$header[0] = 'Authorization: mundiovectone F2SeWBiCUvVmO6z66C6qgXNAoHNe4YB9aIW3nK78zxw=';
	$header[1] = 'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4';
	$header[2] = 'Content-MD5: 917200022538';
	$header[3] = 'User-Agent: mundiovectone';
	$header[4] = 'Accept: application/json';
	$header[5] = 'Accept-Charset: UTF-8';
	$header[6] = 'Content-Type: application/json';
	$header[7] = 'Host: 192.168.2.102:9706';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
	if ($data) {
		$str = json_encode($data);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	}
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	
	//echo "<pre>";
	//print_r($header);
	//exit;
    $output = curl_exec($ch);
	curl_close($ch);
    return json_decode($output, true);
}

function ApiServicePostPayM($apiURL, $apiValue) {                   

	$ch = curl_init() ;
	curl_setopt($ch, CURLOPT_URL,$apiURL);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $apiValue);
	 
	$result = curl_exec($ch);
 
if (curl_errno($ch)) {
   echo 'Curl error: ' . curl_error($ch) . "\r\n";
   $info = curl_getinfo($ch);
   print_r($info);
   curl_close($ch);   
   return;
   }
 
curl_close($ch);
return json_decode($result);

}

function ApiGetServiceDetails($apiUrl,$ApiData) {
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl.$ApiData);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            
    $response = curl_exec($ch);
	
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
	$response = json_decode($response,TRUE);
	$response = json_decode($response);
	return $response;
	
}

 ?>
