jQuery(document).ready(function($) {
   
    $('#card_number').validateCreditCard(function(result) {
		    $('.accepted-cards ul li').removeClass('card-active');
            $('.accepted-cards ul').find('#' + (result.card_type == null ? '-' : result.card_type.name) ).addClass('card-active');
			//$('#edit-hdn-card-type').val((result.card_type == null ? '-' : result.card_type.name));
        });
 
	   $('#edit-issue-no').attr('placeholder','Maestro Card Only')
   

    $("#card_number").bind("keydown", function (event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
              // let it happen, don't do anything
              return;
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
   });
   
   
});