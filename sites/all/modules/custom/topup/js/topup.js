(function ($) {
$(document).ready(function(){
	/* BEGIN: Step 1 Validation */

	$("#topup-registration-form input").on('keyup' , function() { 
		$(this).next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});
	/* END: Step 1 Validation */
	
	/* BEGIN: CC Step 2 Validation */
	
	$("#topup-payment-form input").on('keyup' , function() {
		$(this).next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});
	$("#topup-payment-form select").change(function() {
		$(this).parent().parent().parent().next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});	
	/* END: CC Step 2 Validation */
	
	/* BEGIN: Paysafe Step 2 Validation */
	
	$("#topup-paysafe-step2-form input").on('keyup' , function() { 
		$(this).next('.errormsg').hide();
		$("#replace_validation_div").html('');
	});
	/* END: Paysafe Step 2 Validation */	

	//Validate Mobile Number STEP1
	
	
});
})(jQuery);