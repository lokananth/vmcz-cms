<?php

/**
 * Function to API call using Curl lib
 * @param type $arr_param
 * @return type
 */
function myaccount_curl_get_contents($arr_param) {

    $url = $arr_param['url'];
    // Initiate the curl session
    $ch = curl_init();
    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);
    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);
    // Close the curl session
    curl_close($ch);
    // Return the output as a variable 

    return $output;
}

function myaccount_curl_post_contents($arr_param) {/* echo '<pre>'; print_r($arr_param);*/
   
    $apiURL = $arr_param['url'];
    $apiValue = $arr_param['post_value'];
    header("HTTP/1.1 200 OK");
    header("Content-Type: application/json");
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    $output = curl_exec($ch);
    curl_close($ch);

    return json_decode($output, true);
}

// login redirection to centralized my account 
function myaccount_login_redirection_submit($data) { 

	//extract data from the post
	//set POST variables
	$url = VM_MYACCOUNT_LOGIN_REDIRECTION_API;
	$fields = array(
	'phoneNumber' => $data['phoneNumber'],
	'password' => $data['password'],
	);

	$fields_string = json_encode($fields);

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	'Content-Type: application/json',
	'Content-Length: ' . strlen($fields_string))
	);
	//execute post
	$output = curl_exec($ch);
	//close connection
	curl_close($ch);
	return json_decode($output, true);
}
