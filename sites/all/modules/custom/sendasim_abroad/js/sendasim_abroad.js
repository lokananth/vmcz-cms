(function ($) {
	$(document).ready(function(){

		$("#sendasim-abroad-address-form input").on('keyup' , function() { 
			$(this).next('.errormsg').hide();
			$(this).removeClass('error');			
		}); 
		
		$(".fsa_billing_address_chkbox").on('click', function(){
			if($(".fsa_billing_address_chkbox").is(':checked')){
				$(".loader-bg").show();
				$.ajax({
					url: Drupal.settings.basePath + 'sendasim/abroad/getdeliveryaddress',
					type: "POST",
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$(".loader-bg").hide();
						$(".bill_houseno").val( data.houseno );
						$(".bill_street").val( data.street );
						$(".bill_town").val( data.town );
						$(".bill_pin_code").val( data.postcode );
					}
				});
			}else{
				$(".bill_houseno").val('');
				$(".bill_street").val('');
				$(".bill_town").val('');
				$(".bill_pin_code").val('');
			}
		});
		
		$(".dst_country_list").on('change', function(){
			var dst_country = $(this).val();
			$(".span_sim_cost").text('');
			$(".span_delivery_cost").text('');
			$('input[name="delivery_cost"]').val('');
			if(dst_country != ''){
				$(".loader-bg").show();
				$.ajax({
					url: Drupal.settings.basePath + 'sendasim/abroad/getdeliverycost',
					type: "POST",
					data:{
						dst_country: dst_country
					},
					dataType: "json",
					success: function(data) {
						//console.log(data);
						$(".loader-bg").hide();
						if(data[0].errcode == 0){
							$(".span_sim_cost").text('Free');
							$(".span_delivery_cost").text(data[0].Currency_Code + ' ' + data[0].Delivery_Cost );
							$('input[name="delivery_cost"]').val( data[0].Delivery_Cost );
						}
					}
				});
			}
		});

	});
})(jQuery);