<?php

/**
 * Function to API call using Curl lib
 * @param type $arr_param
 * @return type
 */
 
 define('DMNL_PORTIN_GETSRV_PROVIDER', 'http://192.168.1.21:8090/CRSPFPI.svc/NL/GETSRVPROVIDER');
 define('DMNL_PORTIN_GETNO_PROVIDER', 'http://192.168.1.21:8090/CRSPFPI.svc/NL/GETNOPROVIDER/');
 define('DMNL_PORTIN_GETDATE_PROVIDER', 'http://192.168.1.21:8090/CRSPFPI.svc/NL/ProposeReqDate');
 define('DMNL_PORTIN_REQUEST_SUBMIT', 'http://192.168.1.21:8090/CRSPFPI.svc/NL/RequestPortIN');
 
function portInApiPost($apiType, $apiValue) {                   
        $apiURL = $apiType;
		
		$file = fopen("input.txt","w");
		echo fwrite($file,json_encode($apiValue));
		fclose($file);
        $apiHeader = array();                             
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));                                                          
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));  
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch,CURLOPT_TIMEOUT, 180);
		//curl_setopt($ch,CURLOPT_TIMEOUT, 600);
        $output = curl_exec($ch);
		$file = fopen("output.txt","w");
		echo fwrite($file,json_encode($output));
		fclose($file);
        curl_close($ch);
        return json_decode($output, true);
}

function portInApiGet($arr_param) {

    $url = $arr_param;
    // Initiate the curl session
    $ch = curl_init();
    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);
    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);
    // Close the curl session
    curl_close($ch);
    // Return the output as a variable 

    return json_decode($output,true);
}

function portInApiGetNo($arr_param,$data) {

    $url = $arr_param.$data;
    // Initiate the curl session
    $ch = curl_init();
    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);
    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);
    // Close the curl session
    curl_close($ch);
    // Return the output as a variable 

    return json_decode($output,true);
}

function portInApiGetDate($arr_param,$data) {

    $url = $arr_param.$data;
    // Initiate the curl session
    $ch = curl_init();
    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);
    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);
    // Close the curl session
    curl_close($ch);
    // Return the output as a variable 

    return json_decode($output,true);
}


