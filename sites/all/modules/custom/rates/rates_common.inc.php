<?php

/**
 * Country List 
 * @return type
 */
function get_rates_country_list() {
	global $config;
	$countryOptions = array();
	$param = array(
		"sitecode" => $config['sitecode'],
		"brand" => $config['brand']
	);	
	$response = (object) json_decode(apiPost($config['myaccount_api_endpoint'] . "web_llom_getCountryList_myaccount", $param), true);;
	//echo '<pre> URL : '.$config['myaccount_api_endpoint'].'<br>'; print_r($param); print_r($response); echo '</pre>';
	$countryOptions[] = "Select";
	foreach($response as $country){
		$countryOptions[ucwords(strtolower($country['country_name']))] = ucwords(strtolower($country['country_name']));
	}
	return $countryOptions;
}
/**
* Get National Rates for Current Website
*/
function get_national_rates() {
	global $config;
	$countryOptions = array();
	$data['countrycode'] = $config['rates_countrycode'];
	$data['sitecode'] = $config['sitecode'];
	$data['brand'] = $config['brand'];

	$response = ApiPostNew(VM_NATIONAL_RATES, $data);
	//echo '<pre> URL : '.API_HOST . $_SESSION['Access_Token'] . VM_NATIONAL_RATES.'<br>'; print_r($data); print_r($response); echo '</pre>'; exit;
	return $response;
}


/**
* Get Cheap Call Rates for Current Website
*/
function get_cheep_call_rates() {
	global $config;
	global $base_url;

	$node = node_load(arg(1));
	$term = taxonomy_term_load($node->field_cheapcalls_country['und'][0]['tid']);
	$country_name=$term->name; 
	$data['countrycode'] = $config['ccode'][$country_name];
	$data['Website'] = $config['product'];
	
	$response = ApiPostNew(VM_CHEAPCALL_RATES, $data);
	//echo '<pre> URL : '.API_HOST . $_SESSION['Access_Token'] . VM_CHEAPCALL_RATES.'<br>'; print_r($data); print_r($response); exit;

	$varResult =  '<h2>'.t("Call").' '.$country_name.' '.t("for only:") .'</h2>
				<div class="panel panel-default">
				<div class="panel-body">
                    <ul class="list-inline">
                        <li>
                            <h1 id="ctl00_MainContent_lbllandline" class="cheapcallPrice">'.$response[0]['landline'].'</h1>
                            <span>'.t("landlines") .'</span>
                        </li>
                        <li>
                            <h1 id="ctl00_MainContent_lblMobile" class="cheapcallPrice">'.$response[0]['Mobiles'].'</h1>
                            <span>'.t("mobiles") .'</span>
                        </li>
                    </ul>
					</div>
					</div>
                    <div class="cheapcall_flag">
                        <img alt="cheap calls to '.strtolower($country_name).'" src="'.$base_url.'/sites/default/files/pictures/country_flags/118x82/'.strtolower($country_name).'.png">
                        <img src="'.$base_url.'/sites/default/files/pictures/country_flags/118x82/'.strtolower($config['country']).'.png">
                    </div>
  ';

$form['cheap-call-rates'] = array(
		'#markup' => $varResult
	);
	return $form;
}
