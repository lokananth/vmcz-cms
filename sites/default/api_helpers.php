<?php

function get_url_with_language($target_url){
	global $language;
	global $base_url;
	//get the current language
	$current_lang = $language->language;
	//get the default language
	$default_language = language_default();
	
	$target_url = ltrim($target_url, "/");
	
	switch($current_lang) {
	  case($default_language->language):
		$url = $base_url."/".$target_url;
		break;
	  default:
		$url = $base_url."/". $current_lang ."/".$target_url;
	}
	
	return $url;
}

function ajax_response_url_redirect($redirect_url){
	/* ctools_include('ajax');
	ctools_add_js('ajax-responder');
	$commands[] = ctools_ajax_command_redirect($redirect_url);
	return ajax_render($commands); */
	
	$commands[] = array(
		'command' => 'customAjaxRedirect',
		'url' => $redirect_url
	);
	return ajax_render($commands); 	
}

function validate_mobile_number($mobile){
	
	global $config;
	if(substr($mobile,0,5) == "00{$config['dialing_code']}"){
		$mobile_no = ltrim($mobile, "00{$config['dialing_code']}");
	}else if(substr($mobile,0,4) == "00{$config['dialing_code']}"){
		$mobile_no = ltrim($mobile, "00{$config['dialing_code']}");
	}else if(substr($mobile,0,4) == "0{$config['dialing_code']}"){
		$mobile_no = ltrim($mobile, "0{$config['dialing_code']}");
	}else  if(substr($mobile,0,3) == "0{$config['dialing_code']}"){
		$mobile_no = ltrim($mobile, "0{$config['dialing_code']}");
	}else if(substr($mobile,0,3) == $config['dialing_code']){		
		$mobile_no = ltrim($mobile, $config['dialing_code']);
	}else if(substr($mobile,0,2) == $config['dialing_code']){		
		$mobile_no = ltrim($mobile, $config['dialing_code']);
	}else if(substr($mobile,0,2) == "00"){
		$mobile_no = ltrim($mobile, "00");
	}else if(substr($mobile,0,1) == "0"){
		$mobile_no = ltrim($mobile, "0");
	}else{
		$mobile_no = trim($mobile);
	}
	
	return $config['dialing_code'].$mobile_no;
}
 
 function apiGetKey($username) {
    $isAuth = true;
    //$headers[0] = 'Content-MD5: 917200022538';
    $headers[0] = 'Content-MD5: ' . $username;
    $headers[1] = 'User-Agent: mundiovectone';
    $headers[2] = 'Accept: application/json';
    $headers[3] = 'Accept-Charset: UTF-8';
    $headers[4] = 'Content-Type: application/json';
    $headers[5] = 'Host: 192.168.2.102:9705';
    $headers[6] = 'Content-Length: 0';
    
    if (!$isAuth){
        $headers = array();
    }
    
    $apiUrl = config_item('vrp_api_endpoint')."Key";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    if ($isAuth){
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}

function apiPostLogin($apiUrl, $data = Array(), $isAuth = true) {
              
	$header[0] = 'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=';
	$header[1] = 'Content-MD5:917200022538';
	$header[2] = 'Accept: application/json';
	$header[3] = 'Content-Type: application/json';
	$header[4] = 'crossDomain: true';
	if (!$data) {
		$header[5] = 'Content-Length: 0';
	}
                
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
   
   if ($data) {
        $str = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
    
    return json_decode($output, true);
}


/**
 * Function to API call using Curl lib
 * @param type $arr_param
 * @return type
 */
 
 function apiPost($apiUrl, $data, $isAuth = true) {

  // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: webapi.vectone.com'
    ));
   // execute the request
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function apiGet($apiUrl, $isAuth = true) {
    $headers = array(
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: webapi.vectone.com'
    );
	if (!$isAuth){
		$headers = array();
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	if ($isAuth){
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}



function getApiAccessToken(){
	global $config;
	$data['Proj_ID'] = '101836';
	$apiUrl = API_HOST. 'Mobileapiinserttokenmaster';
	$header[0] = 'Authorization: Basic TXVuZGlvTW9iaWxlY21zYXBpMTAxOA==';	
	
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
   
   if ($data) {
        $str = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
    
    return $output;
}

function apiPostNew($apiUrl, $data = Array(), $isAuth = true) {

	regenerate_token:
	$Access_Token = '';
	if(!isset($_SESSION['Access_Token']) && empty($_SESSION['Access_Token'])){
		$token_result = getApiAccessToken();
		$result = (object) json_decode($token_result, true);
		$Access_Token = $result->tokenlist['AccessTokenID'];
		$_SESSION['Access_Token'] = $Access_Token;
	}
	$Url = API_HOST. $_SESSION['Access_Token'] . $apiUrl;
	
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $Url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
   
   if ($data) {
        $str = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $output = curl_exec($ch);
    curl_close($ch);
	$_SESSION['api_url'] = $Url;
	$_SESSION['in_out_details'] = $str .'~'.$output;
	$result = json_decode($output, true);
	
    if(isset($result['Message']) && $result['Message'] === 'Authorization has been denied for this request.'){
		unset($_SESSION['Access_Token']);
		goto regenerate_token;
	}
	
    return $result;
}
 
function ApiWebPost($apiUrl, $data = Array(), $isAuth = true) {
              
	$header[0] = 'Authorization: mundiovectone F2SeWBiCUvVmO6z66C6qgXNAoHNe4YB9aIW3nK78zxw=';
	$header[1] = 'Mundio-Api-PublicKey: OTE3MjAwMDIyNTM4';
	$header[2] = 'Content-MD5:917200022538';
	$header[3] = 'Accept: application/json';
	$header[4] = 'Content-Type: application/json';
	$header[5] = 'crossDomain: true';
	if (!$data) {
		$header[6] = 'Content-Length: 0';
	}                

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
   
   if ($data) {
        $str = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    $output = curl_exec($ch);
    curl_close($ch);
	
    return json_decode($output, true);
}

function product_code_by_item_id($itemId){
	global $config;
	$data = array(
		'sitecode' => $config['sitecode'],
		'brand' => $config['brand'],
		'item_id' => $itemId
	);
	$response = ApiPostNew(PRODUCT_CODE_BY_ITEM_ID, $data);
	return $response;
}

$user_agent     =   $_SERVER['HTTP_USER_AGENT'];
function getOS() { 

    global $user_agent;
    $os_platform    =   $user_agent;
    $os_array       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );

    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    } 
    return $os_platform;
}

function getBrowser() {
    global $user_agent;
    $browser        =   $user_agent;
    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/edge/i'       =>  'Edge',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser'
                        );

    foreach ($browser_array as $regex => $value) { 
        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }
    }
    return $browser;
}